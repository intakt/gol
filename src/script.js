var worldSize = 64

var worldScale = 8

var A = createWorld(worldSize)
var B = createWorld(worldSize)

var simulating = false

function v2(x, y){
  return {x : x, y : y}
}

var mousePos = v2(0,0)

var dirs = [
  v2(0,-1), 
  v2(1, -1),
  v2(1, 0), 
  v2(1, 1),
  v2(0, 1), 
  v2(-1, 1),
  v2(-1,0), 
  v2(-1, -1)
]
// [
//   [{ living : false }, { living : false }, { living : false }, { living : false }],
//   [{ living : false }, { living : false }, { living : false }, { living : false }],
//   [{ living : false }, { living : false }, { living : false }, { living : false }],
//   [{ living : false }, { living : false }, { living : false }, { living : false }],
// ]

// A[0][1]

function createWorld(w){
  var ym = []
  for(var y = 0; y < w; y++){
    var xm = []
    for (var x = 0; x < w; x++){
      xm.push({ living : Math.random() < 0.0  })
    }
    ym.push(xm)
  }
  return ym
}

function setup(){
  createCanvas(worldSize * worldScale, worldSize * worldScale)

  $("canvas").mousemove((e)=> mousePos = v2(Math.floor(e.pageX / worldScale), Math.floor(e.pageY / worldScale)))
  // fill
}

function draw(){
  // background(255)
  // fill(255)
  if(simulating)
    simulateWorld(A, B)
  
  drawWorld(A)

  noStroke()
  fill(255, 200, 0)
  rect(mousePos.x * worldScale, mousePos.y * worldScale, worldScale, worldScale)
  // console.log(mousePos)
}


function keyPressed(e){
  if(e.keyCode == 32) // space
    simulating = !simulating
}

function mouseDragged(){
  // A[mousePos.x][mousePos.y].living = !A[mousePos.x][mousePos.y].living
}
function mouseClicked(){
  console.log(mousePos)
  A[mousePos.x][mousePos.y].living = !A[mousePos.x][mousePos.y].living
  B[mousePos.x][mousePos.y].living = A[mousePos.x][mousePos.y].living
}

function drawWorld(world){
  for(var x = 0; x < world.length; x++){
    for (var y = 0; y < world.length; y++){
      var r = world[x][y].living ? 255 : 0
      noStroke()
      fill(r, 0, 0)
      rect(x * worldScale, y * worldScale, worldScale, worldScale)
    }
  }
}

function simulateCell(cell, neighbors){
  var livingneighbors = neighbors.reduce((a, v)=> a + (v.living ? 1 : 0), 0)
  // console.log(livingneighbors)
  var l = cell.living
  if(cell.living && (livingneighbors < 2 || livingneighbors > 3))
    l = false
  else if(!cell.living && livingneighbors == 3)
    l = true

  return {living : l}

}

function getNeighbors(world, x, y){
  return dirs.map(function(d){
      let _x = x + d.x < 0 ? world.length - 1 : (x + d.x) % world.length
      let _y = y + d.y < 0 ? world.length - 1 : (y + d.y) % world.length
      return world[_x][_y]
    }
  )
}

function simulateWorld(){
  for(var x = 0; x < A.length; x++){
    for (var y = 0; y < A.length; y++){
      B[x][y] = simulateCell(A[x][y], getNeighbors(A, x, y))
    }
  }

  var c = A
  A = B
  B = c
}
